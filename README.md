Example code from the [telematics lecture](https://telematics.tm.kit.edu/) at KIT, for experiments at home.

Check out the individual READMEs for each example.