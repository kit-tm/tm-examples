        Beispiel zur Verwendung des asn1c

(0) asn1c kompilieren und installieren
  -> http://lionet.info/asn1c

(1) ASN.1 Datei kompilieren:
        $ asn1c -fnative-types rectangle.asn1

(2) Datei main.c ggf. mit konkreten Werten anpassen

(3) Binary erstellen, welches uns die BER (und XER) Kodierung liefert
        $ rm converter-sample.c
        $ cc -I. -o rencode *.c

(4) BER-Datei erstellen (in Datei "ber-sample")
        $ ./rencode ber-sample

(5) Hexdump der Datei "ber-sample" ausgeben lassen
        $ hexdump -C ber-sample
  00000000  30 06 02 01 2a 02 01 17                           |0...*...| 
  00000008

