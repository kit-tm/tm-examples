	Beispiel zur Verwendung von Google Protocol Buffers

(0) Protocol Buffers kompilieren und installieren
  -> https://developers.google.com/protocol-buffers

(1) .proto Datei kompilieren:
	$ protoc --cpp_out=. rectangle.proto

(2) Datei main.cc ggf. mit konkreten Werten anpassen

(3) Binary erstellen, welches uns die BER (und XER) Kodierung liefert
	$ c++ *.cc -o rencode -lprotobuf

(4) BER-Datei erstellen (in Datei "protobuf")
	$ ./rencode protobuf

(5) Hexdump der Datei "protobuf-sample" ausgeben lassen
  $ hexdump -C protobuf-sample
  00000000  08 2a 10 17                                       |.*..|
  00000004
