#include <iostream>
#include <fstream>

#include "rectangle.pb.h"

using namespace std;

int main(int argc, char ** argv) {

  if(argc < 2) return -1;
  fstream out(argv[1], ios::out | ios::binary | ios::trunc);  
  if(!out.is_open()) return -1;

  Rectangle rect;
  rect.set_height(42);
  rect.set_width(23);

  rect.SerializeToOstream(&out);
  out.close();
      
  return 0; 
}
